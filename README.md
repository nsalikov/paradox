# Paradox2MySQL Script

Script for import from Paradox's database to MySQL.

## Getting Started

Directory tree:

* `~/projects/paradox` -- project's directory
* `~/projects/paradox/bin/mmautogd` -- script itself
* `~/projects/paradox/data` -- data directory
* `~/projects/paradox/data/MMAutoGD.Db` -- you should put Paradox's database here
* `~/projects/paradox/data/pre.sql` -- sql code for preprocessing (currently, DROP TABLE IF EXISTS...)
* `~/projects/paradox/data/post.sql` -- sql code for postrocessing (alter table, delete or modify columns and so on; can be empty)

### Configuration

Set variable `data_dir` to data directory:

```
data_dir="/home/c4rtradersa/projects/paradox/data"
```

Tweak this if need be:

```
pxview="pxview --output-file=\"${data_dir}/${data}\" --mode=sql --tablename=mmautogd --fields=\"MMCODE|MAKE|MODEL|NOCYL|CUBICCAP|KW|BODY|DOORS|MASS|INTRODATE|PRICE|OPT|VTYPE|HIGH|LOW|NUMSOLD|GVM|AXLE|WHEELBASE|DORP|TYRES|TYRES2|AOption\" \"${data_dir}/${paradox_db}\""

# Don't use MySQL password in command line.
# Otherwise, a warning message will be generated and cron will send email to you.
# Redirecting STDERR (2>/dev/null) is not option, since	you should know	if script fails.

export MYSQL_PWD=dEwOm5gB4Y6O
mysql="mysql --user=c4rtrade_auto c4rtrade_mmautogd"
```
## Usage

Run script through cron or terminal.
